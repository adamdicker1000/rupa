defmodule Rupa.Repo.Migrations.CreateUserTable do
  use Ecto.Migration

  def change do
    create table(:Users) do
      add(:points, :integer, null: false)

      timestamps()
    end

    create index(:Users, [:points])
    create constraint(:Users, :points_range_constraint, check: "points >= 0 and points <= 100")
  end
end
