# Bulk insert 1,000,000 users in batches of 20,000. Postgres
# only supports ~65k params per query and we have three params
# per user (points + timestamps) so we need to batch.

now = NaiveDateTime.truncate(NaiveDateTime.utc_now(), :second)

for _ <- 0..49 do
  users =
    for _ <- 0..19_999,
        do: %{points: 0, inserted_at: now, updated_at: now}

  Rupa.Repo.insert_all(Rupa.User, users)
end
