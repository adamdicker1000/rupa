# Random User Point Allocator (Rupa)

Provides a solution to the problem defined [here](https://www.notion.so/Backend-code-exercise-d7df215ccb6f4d87a3ef865506763d50).

To run the application:
  * Install dependencies with `mix deps.get`
  * Set up the database with `mix ecto.setup`
  * Start Phoenix endpoint with `mix phx.server`

This will launch the server on [`localhost:4000`](http://localhost:4000).

To run the tests:
  * Set up the database with `MIX_ENV=test mix ecto.create && mix ecto.migrate`
  * Run the tests with `mix test`

To generate the docs:
  * Generate the docs with `mix docs`
  * Open the docs at `./docs/index.html`

# Notes and considerations
* Bulk inserts were used in the seed script for efficiency. Inserting
1 million records sequentially is sloooow.
* [`update_all/3`](https://hexdocs.pm/ecto/Ecto.Repo.html#c:update_all/3) is 
used with database side random generation to allocate points to the users.
This is fast as it avoids the read / update / write pattern but we lose 
the observability and testability of generating the random numbers application
side. The trade off here was deemed worth the performance gain.
* Updates on this scale / frequencey will generate a lot of record
bloat because Postgres essentially keeps old rows around for 
efficient rollbacks. If this were a production setting, we would
likely have to have a pretty aggressive `VACCUUM` scheduling policy.
* Point allocation is run as an async task in the allocator.
This is done to avoid blocking the inbound requests while updating.
If the update query execution time is greater than the refresh rate,
we will potentially have multiple queries executing at the same time.
This will eventually cause issues becuase of the scale of the upate query.
To avoid this, the allocator will skip udpates until all previous
quieries have finished.
* The codebase is just the output of running `mix phx.new`. A lot of 
fluff has been left in for expediency, but to make this a truly production
ready app we would have to tidy up `config/`, remove unwanted features
from the endpoint etc. 
