defmodule Rupa.User do
  use Ecto.Schema

  @moduledoc """
  The `Users` table data model.
  """

  @type t :: %__MODULE__{
          id: integer,
          points: integer,
          inserted_at: NaiveDateTime.t(),
          updated_at: NaiveDateTime.t()
        }

  schema "Users" do
    field(:points, :integer)

    timestamps()
  end
end
