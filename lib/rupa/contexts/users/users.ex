defmodule Rupa.Users do
  import Ecto.Query

  alias Rupa.{Repo, User}

  @moduledoc """
  Provides logic and queries for the `Users` table.
  """

  @doc """
  Queries the database for users whose points
  are greater than `max_number`, limiting the result
  set to `limit`.
  """
  @spec in_range(integer, integer) :: list(User.t())
  def in_range(max_number, limit) do
    User
    |> where([u], u.points > ^max_number)
    |> limit(^limit)
    |> Repo.all()
  end

  @doc """
  Updates all users to have a random points value
  in the range of [0, max_numbers].
  """
  @spec update_points(integer) :: {:ok, integer}
  def update_points(max_number) do
    {count, _data} =
      User
      |> update(set: [points: fragment("floor(random() * (? + 1))", ^max_number)])
      |> Repo.update_all([])

    {:ok, count}
  end
end
