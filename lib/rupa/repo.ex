defmodule Rupa.Repo do
  use Ecto.Repo,
    otp_app: :rupa,
    adapter: Ecto.Adapters.Postgres

  @moduledoc false
end
