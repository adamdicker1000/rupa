defmodule Rupa.Allocator do
  use GenServer

  alias Rupa.Users

  @moduledoc """
  Provides the base functionality of the application, serving
  requests for user data and allocating user points.

  To understand the problem being solved, view the [specs](https://www.notion.so/Backend-code-exercise-d7df215ccb6f4d87a3ef865506763d50).

  Point allocation will be run at an interval of 60 seconds,
  and requests for user data will be limited to 2 records.

  Rather than potentially blocking the GenServer during
  expensive queries, point allocation occurs on a background
  process. This means it is technically feasible that multiple
  point allocation queries can be ran concurrently, if a
  previous query is still running when a new allocation occurs.
  To prevent this, an allocation will be skipped if there is
  an allocation ongoing.
  """

  @max_point_range 100
  @refresh_rate_ms 60_000
  @response_limit 2

  @doc """
  The entry point to the GenServer.

  ### Options
    * `:max_number` - the initial seed. Use to ensure
    determinism during tests. Defaults to a random integer
    in the range [0, 100].

    * `:skip_allocate` - If `true`, the server
    will not launch the point allocation procedure. Use to
    ensure determinism during tests. Defaults to `false`.
  """

  @spec start_link(Keyword.t()) :: GenServer.on_start()
  def start_link(opts \\ []),
    do: GenServer.start_link(__MODULE__, opts, name: __MODULE__)

  @impl true
  def init(opts) do
    unless Keyword.get(opts, :skip_allocate, false),
      do: scheule_allocate()

    max_number = Keyword.get(opts, :max_number, generate_max_number())

    {:ok, %{max_number: max_number, timestamp: nil}}
  end

  @doc """
  Fetches a maximum of 2 users whose points exceed the current `max_number`
  stored in the servers state. Will also return the `timestamp` in `UTC` of
  the previous call to the function.
  """
  @spec fetch_users() :: %{
          required(:timestamp) => NaiveDateTime.t(),
          required(:users) => list(Rupa.User.t())
        }
  def fetch_users(),
    do: GenServer.call(__MODULE__, :fetch_users)

  @impl true
  def handle_call(:fetch_users, _from, %{timestamp: timestamp, max_number: max_number} = state) do
    response = %{
      timestamp: timestamp,
      users: Users.in_range(max_number, @response_limit)
    }

    {:reply, response, %{state | timestamp: NaiveDateTime.utc_now()}}
  end

  @impl true
  def handle_info(:allocate_points, %{allocate_task: _} = state) do
    scheule_allocate()

    {:noreply, state}
  end

  def handle_info(:allocate_points, state) do
    state =
      state
      |> Map.put(:max_number, generate_max_number())
      |> Map.put(:allocate_task, Task.async(fn -> Users.update_points(@max_point_range) end))

    scheule_allocate()

    {:noreply, state}
  end

  # Close the child task in the DOWN message not the task response in case the task fails
  def handle_info({:DOWN, ref, :process, _, _}, %{allocate_task: %Task{ref: ref}} = state),
    do: {:noreply, Map.delete(state, :allocate_task)}

  def handle_info(_message, state), do: {:noreply, state}

  defp generate_max_number(),
    do: :rand.uniform(@max_point_range + 1) - 1

  defp scheule_allocate(),
    do: Process.send_after(self(), :allocate_points, @refresh_rate_ms)
end
