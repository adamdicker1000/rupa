defmodule Rupa.Application do
  use Application

  @moduledoc false

  def start(_type, _args) do
    children = [
      Rupa.Repo,
      RupaWeb.Endpoint
    ]

    # We don't want the Allocator attached to the supervision
    # tree during tests.
    children =
      case Mix.env() do
        :test -> children
        _else -> children ++ [{Rupa.Allocator, []}]
      end

    opts = [strategy: :one_for_one, name: Rupa.Supervisor]

    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    RupaWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
