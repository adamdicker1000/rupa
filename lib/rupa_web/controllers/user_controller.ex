defmodule RupaWeb.UserController do
  use RupaWeb, :controller

  @moduledoc false

  alias Rupa.Allocator

  def index(conn, _params) do
    conn
    |> put_status(:ok)
    |> render("index.json", Allocator.fetch_users())
  end
end
