defmodule RupaWeb.Router do
  use RupaWeb, :router
  use Plug.ErrorHandler

  @moduledoc false

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", RupaWeb do
    pipe_through :api

    get "/", UserController, :index
  end

  # Phoenix natively renders HTML errors when processing unhandled exceptions.
  # For json API's this is unwanted, so we pretty print all errors as json
  # via the ErrorView.
  def handle_errors(conn, %{kind: _kind, reason: _reason, stack: _stack}) do
    json(conn, RupaWeb.ErrorView.render("#{conn.status}.json"))
  end
end
