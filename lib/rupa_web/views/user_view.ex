defmodule RupaWeb.UserView do
  use RupaWeb, :view

  @moduledoc false

  def render("index.json", %{users: users, timestamp: timestamp}) do
    %{
      users: render_many(users, __MODULE__, "user.json"),
      timestamp: render_timestamp(timestamp)
    }
  end

  def render("user.json", %{user: user}),
    do: Map.take(user, [:id, :points])

  defp render_timestamp(nil), do: nil

  defp render_timestamp(timestamp) do
    timestamp
    |> NaiveDateTime.truncate(:second)
    |> NaiveDateTime.to_string()
  end
end
