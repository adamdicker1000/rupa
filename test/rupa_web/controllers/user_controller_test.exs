defmodule RupaWeb.UserControllerTest do
  use RupaWeb.ConnCase, async: false

  alias Rupa.{Allocator, Repo, User}

  describe "GET /" do
    defp call(conn) do
      conn
      |> get(Routes.user_path(conn, :index))
      |> json_response(200)
    end

    test "returns 200 with data", %{conn: conn} do
      {:ok, _pid} = Allocator.start_link(skip_allocate: true, max_number: 0)

      Repo.insert(%User{points: 100})
      Repo.insert(%User{points: 100})

      assert %{
               "timestamp" => nil,
               "users" => [%{"id" => _, "points" => 100}, %{"id" => _, "points" => 100}]
             } = call(conn)

      assert %{
               "timestamp" => timestamp,
               "users" => [%{"id" => _, "points" => 100}, %{"id" => _, "points" => 100}]
             } = call(conn)

      assert is_binary(timestamp)
    end
  end
end
