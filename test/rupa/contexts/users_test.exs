defmodule Rupa.UsersTests do
  use Rupa.DataCase, async: true

  alias Rupa.{Repo, User, Users}

  describe "database constrains" do
    test "points to range [0, 100]" do
      for points <- [-1, 101] do
        assert_raise Ecto.ConstraintError, ~r/points_range_constraint/, fn ->
          Repo.insert(%User{points: points})
        end
      end
    end
  end

  describe "in_range/2" do
    test "returns users with more points than max_number" do
      {:ok, user} = Repo.insert(%User{points: 100})

      assert [^user] = Users.in_range(99, 1)
    end

    test "returns empty list" do
      assert [] = Users.in_range(100, 1)
    end

    test "limits the result set" do
      Repo.insert(%User{points: 100})
      Repo.insert(%User{points: 100})

      assert [%User{}] = Users.in_range(99, 1)
    end
  end

  describe "update_points/1" do
    test "updates user points to random value in range [0, max_number]" do
      Repo.insert(%User{points: 1})

      assert {:ok, 1} = Users.update_points(0)

      assert [%User{points: 0}] = Repo.all(User)
    end
  end
end
