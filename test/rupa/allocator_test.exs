defmodule Rupa.AllocatorTest do
  use Rupa.DataCase, async: false

  alias Rupa.{Allocator, Repo, User}

  describe "fetch_user/0" do
    test "refreshes timestamp" do
      {:ok, _pid} = Allocator.start_link(skip_allocate: true)

      assert %{timestamp: nil} = Allocator.fetch_users()
      assert %{timestamp: %NaiveDateTime{} = timestamp} = Allocator.fetch_users()
      assert timestamp <= NaiveDateTime.utc_now()
    end

    test "limits results set to 2" do
      {:ok, _pid} = Allocator.start_link(skip_allocate: true, max_number: 0)

      Repo.insert(%User{points: 100})
      Repo.insert(%User{points: 100})
      Repo.insert(%User{points: 100})

      assert %{users: [%User{}, %User{}]} = Allocator.fetch_users()
    end
  end
end
